from flask_bcrypt import Bcrypt
from flask_mongoengine import MongoEngine

bcrypt = Bcrypt()
db = MongoEngine()
