from flask import Flask

from extensions import bcrypt, db
from notes_app.views import AddNoteView, ShowNoteView

app = Flask(__name__)
app.config.from_pyfile('settings.cfg')
bcrypt.init_app(app)
db.init_app(app)

app.add_url_rule('/', view_func=AddNoteView.as_view('notes'))
app.add_url_rule('/notes/<string:note_id>', view_func=ShowNoteView.as_view('show_note'))

if __name__ == '__main__':
    app.run()
