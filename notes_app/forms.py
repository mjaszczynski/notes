from wtforms import (
    Form,
    IntegerField,
    PasswordField,
    StringField,
    ValidationError,
    validators,
)
from wtforms.widgets import TextArea, TextInput

from extensions import bcrypt


class AddNoteForm(Form):
    title = StringField('Title', [validators.Length(min=1, max=50)], widget=TextInput())
    note = StringField('Note', [validators.Length(min=1, max=5000)], widget=TextArea())
    password = PasswordField('Password', [
        validators.Length(min=5, max=32),
        validators.EqualTo('confirm', message='Passwords must match')
    ])
    confirm = PasswordField('Repeat Password')
    ttl = IntegerField(
        'Time (in minutes) after which this note will expire. Setting 0 disables this option.',
        [validators.NumberRange(min=0, max=60*24*7)],  # up to one week
        default=0,
    )


class PasswordCheckForm(Form):
    password = PasswordField('Password')
    current_password = ''

    def validate_password(self, field):
        if not bcrypt.check_password_hash(self.current_password, field.data):
            raise ValidationError('Incorrect password')
