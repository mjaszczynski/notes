from extensions import bcrypt, db


class Note(db.Document):
    title = db.StringField(min_length=1, max_length=120)
    note = db.StringField(min_length=1, max_length=3000)
    password = db.StringField()
    expire_at = db.DateTimeField()
    meta = {
        'indexes': [
            {
                'name': 'TTL_index',
                'fields': ['expire_at'],
                'expireAfterSeconds': 0,
            },
        ],
    }

    def clean(self):
        self.password = bcrypt.generate_password_hash(self.password).decode('utf-8')
