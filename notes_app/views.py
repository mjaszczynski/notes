from datetime import datetime, timedelta

from flask import render_template, request, url_for
from flask.views import MethodView

from notes_app.forms import AddNoteForm, PasswordCheckForm
from notes_app.models import Note


class AddNoteView(MethodView):
    def get(self):
        form = AddNoteForm()
        return render_template('new_note.html', form=form)

    def post(self):
        form = AddNoteForm(request.form)
        if form.validate():
            note = Note(
                title=form.title.data,
                note=form.note.data,
                password=form.password.data,
            )
            if form.ttl.data > 0:
                note.expire_at = datetime.utcnow() + timedelta(seconds=form.ttl.data * 60)
            note.save()

            return render_template('note_added.html', url=url_for('show_note', note_id=note.id, _external=True))
        return render_template('new_note.html', form=form)


class ShowNoteView(MethodView):
    def get(self, note_id: int):
        Note.objects.get_or_404(id=note_id)
        form = PasswordCheckForm()
        return render_template('note_password.html', form=form)

    def post(self, note_id: int):
        note = Note.objects.get_or_404(id=note_id)
        form = PasswordCheckForm(request.form)
        form.current_password = note.password
        if form.validate():
            return render_template('view_note.html', note=note)
        return render_template('note_password.html', form=form)
