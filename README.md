### Running the code
In order to start the application and MongoDB use docker-compose:

```docker-compose up```

Application will be available on ```localhost:5000```.
